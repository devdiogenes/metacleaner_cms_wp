=== Metawash: Metadata nice and clean ===
Plugin Name: Metawash
Contributors: javimoncayo
Web: Suments Data https://www.suments.com
Stable tag: 7.0.0
Tags: GDPR,compliance, exif data,data protection, privacy, information, cibersecurity,  eliminar metadatos, data protection,data leak,data breach
Donate link: https://www.metawash.me
Requires at least: 3.0
Requires PHP: 4.0
Tested up to: 6.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Remove metadata data after uploading an image to your WP. Supported format: JPG and PNG.

== Description ==

Remove metadata data after uploading an image to your WP. Supported format: JPG and PNG.

== Installation ==

1. Upload the directory 'Suments Metacleaner FREE version.' to the '/wp-content/plugins/' directory
2. Activate the plugin on 'Plugins' tab
3. Enjoy

== PHP ==
>7.8

== Changelog ==

= 7.1.0 =

== Upgrade Notice ==
None

== Frequently Asked Questions ==
None

== Screenshots ==
1. This is the main admin panel

= 7.1.0 =
None.
